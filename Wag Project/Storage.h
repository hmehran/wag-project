//
//  Storage.h
//  Wag Project
//
//  Created by Hamoon Mehran on 4/4/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Storage : NSObject

-(void)saveImage:(UIImage *)image username:(NSString *)username;
-(UIImage *)getImage:(NSString *)username;

@end
