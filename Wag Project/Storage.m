//
//  Storage.m
//  Wag Project
//
//  Created by Hamoon Mehran on 4/4/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

#import "Storage.h"

@implementation Storage

-(UIImage *)getImage:(NSString *)username
{
    
     NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_profileimage.jpeg",username]];
     UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    
    return image;
   
    
}

-(void)saveImage:(UIImage *)image username:(NSString *)username
{
    //Save Image to NSTemporary Directory
    NSString *filename = [NSString stringWithFormat:@"%@_profileimage.jpeg", username];
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 100);
    [imageData writeToURL:fileURL atomically:YES];
}







/* Extra
 
 /*
 //Save Image to NSTemporary Directory
 NSString *filename = [NSString stringWithFormat:@"%@_image.jpeg", username];
 NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
 NSURL *fileURL = [NSURL fileURLWithPath:filePath];
 
 NSData *imageData = UIImageJPEGRepresentation(image, 100);
 [imageData writeToURL:fileURL atomically:YES];

 */

/*
 NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_image.jpeg",username]];
 UIImage *image = [UIImage imageWithContentsOfFile:filePath];
 */


@end
