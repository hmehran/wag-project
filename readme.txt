Project Title
This is Hamoon Mehran's Interview Wag Project

Getting Started
1. Simply Download the repository from https://bitbucket.org/hmehran/wag-project/downloads/
2. Open project in Xcode
3. Run the Project 

Prerequisites
You need to have Xcode installed prior to running this project.


Built With
Xcode - IOS Framework
Objective-C - IOS Programming Language


Versioning
We use Bitbucket.org for versioning. For the versions available, see the commits on this repository. https://bitbucket.org/hmehran/wag-project/downloads/

Authors
Hamoon Mehran

License
This project is licensed under the MIT License 

Acknowledgments
Shout out to my puppy for keeping me company while I was coding this. Love ya. 