//
//  ViewController.m
//  Wag Project
//
//  Created by Hamoon Mehran on 4/3/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

#import "ViewController.h"
#import "Storage.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIActivityIndicatorView *loadingAnimation;
@property (strong, nonatomic) NSArray *userArray;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
}

-(void)setUp
{
    self.userArray = nil;
    
    self.loadingAnimation = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadingAnimation.center = CGPointMake(160, 240);
    self.loadingAnimation.frame = CGRectMake(self.view.bounds.size.width/2 - 25, 100, 50, 50);
    [self.view addSubview:self.loadingAnimation];
    
    self.navigationItem.title = @"Stack Overflow Users";
    
    [self getStackOverflowJson];
    
}

#pragma mark - Loading Animation
-(void)startLoadingAnimation
{
    //startLoadingAnimation must be called from main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.loadingAnimation startAnimating];
        [self.loadingAnimation setHidden:NO];
    });
    
}

-(void)endLoadingAnimation
{
    //endLoadingAnimation must be called from main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.loadingAnimation stopAnimating];
        [self.loadingAnimation setHidden:YES];
    });
    
}


#pragma mark - HTTP Methods

-(void)getStackOverflowJson
{
    
    NSURL *url = [NSURL URLWithString:@"https://api.stackexchange.com/2.2/users?site=stackoverflow"];
    
    NSURLSessionDataTask *getRequest = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        
        NSError *jsonError;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:nil error:&jsonError];
        
        self.userArray = [jsonResponse objectForKey:@"items"];
        
        /* testing
         //NSLog(@"%@",[jsonResponse objectForKey:@"items"]);
        NSLog(@"jsonResponse: %@", self.userArray );
        NSLog(@"display name: %@", [self.userArray [0] objectForKey:@"display_name"]);
        NSLog(@"badge counts: %@", [self.userArray [0] objectForKey:@"badge_counts"]);
        NSLog(@"profile image: %@", [self.userArray [0] objectForKey:@"profile_image"]);
         */
        
        
        [self reloadTableView];
        
       
    }];
    
    [getRequest resume];
    
    
    
}


-(void)getStackOverflowPortraitImages:(NSString *)imageURL username:(NSString *)username
{
    NSURL *url = [NSURL URLWithString:imageURL];
    NSURLSessionDownloadTask *downloadImage = [[NSURLSession sharedSession] downloadTaskWithURL:url completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
        
        Storage *storage = [[Storage alloc] init];
        [storage saveImage:image username:username];
        
        [self endLoadingAnimation];
        [self reloadTableView];
        
    }];
    
    [downloadImage resume];
    
}

-(void)reloadTableView
{
    //reload data must be called from a main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


#pragma mark - TableView Methods


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.userArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cell_identifier];
    
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cell_identifier];
        
    }
    
    //Important Check to see if userArray has anything inside of it.. Otherwise calls to it will make the app crash
    if(self.userArray != nil & self.userArray.count > 0)
    {
        //#################
        //1. Username #####
        //#################
        NSString *username = [self.userArray[indexPath.row] valueForKey:@"display_name"];
        
        //Very important check to keep the program from catching, when the database happens to return null
        if(username == (id)[NSNull null]){
            cell.textLabel.text = @"";
        }else {
            cell.textLabel.text = username;
        }
       
        
        //#################
        //2. Badge Counts ##
        //#################
        
        
        NSString *bronze = [[self.userArray[indexPath.row] objectForKey:@"badge_counts"] valueForKey:@"bronze"];
        NSString *silver = [[self.userArray[indexPath.row] objectForKey:@"badge_counts"] valueForKey:@"silver"];
        NSString *gold = [[self.userArray[indexPath.row] objectForKey:@"badge_counts"] valueForKey:@"gold"];
        
        //Very important check to keep the program from catching, when the database happens to return null
       if(bronze == (id)[NSNull null] || silver == (id)[NSNull null] || gold == (id)[NSNull null] )
       {
           cell.detailTextLabel.text = @"";
       }else
       {
           //then nsmutableattributed string
           NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@• %@• %@•", gold, silver, bronze]];
           
           NSInteger dot1Location = [self searchStringForDot:attributedString.string count:1];
           NSInteger dot2Location = [self searchStringForDot:attributedString.string count:2];
           NSInteger dot3Location = [self searchStringForDot:attributedString.string count:3];
           
           [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1.00 green:0.81 blue:0.25 alpha:1.00] range:NSMakeRange(dot1Location, 1)];
           [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:1.00] range:NSMakeRange(dot2Location, 1)];
           [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.80 green:0.50 blue:0.20 alpha:1.00] range:NSMakeRange(dot3Location, 1)];
           
           
           cell.detailTextLabel.attributedText = attributedString;
           
       }
        
        
                                                  
        

        //####################
        //3. Profile Images ##
        //####################
        
        ///cell.imageView.image = [UIImage imageNamed:@"test"];
        
        NSString *imagename = [self.userArray[indexPath.row] valueForKey:@"profile_image"];
        
        //Very important check to keep the program from catching, when the database happens to return null
        if(imagename == (id)[NSNull null] )
        {
            //do nothing
             cell.imageView.image = [UIImage imageNamed:@"test"];
        }
        else
        {
            
            Storage *storage= [[Storage alloc] init];
            UIImage *image = [storage getImage:username];
           
            
            
            //1st check to see if we have image already saved in the directory
            if(image != nil)
            {
                NSLog(@"worked");
                cell.imageView.image = image;
            }
            //2nd if not then proceed to download image and save it to the directory
            else
            {
                
                [self getStackOverflowPortraitImages:imagename username:username];
                //Starts Loading Animation while downloading image
                [self startLoadingAnimation];
            }
            
            
            
        }
        
        
        
        
    }
    
   
    
    return cell;
}


//helper method used for searching for the dot locations in the string
-(NSInteger)searchStringForDot:(NSString *)string count:(NSInteger)count
{
    NSInteger counter = 1;
    
    for (int i = 0; i < string.length; i++)
    {
        NSString *character = [string substringWithRange:NSMakeRange(i, 1)];
        if([character isEqualToString:@"•"])
        {
            if(counter == count)
            {
                return i;
            }
            else{
                counter += 1;
            }
            
        }
    }
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Unused for now
}

#pragma mark - Extra Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
