//
//  AppDelegate.h
//  Wag Project
//
//  Created by Hamoon Mehran on 4/3/18.
//  Copyright © 2018 Hamoon Mehran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

